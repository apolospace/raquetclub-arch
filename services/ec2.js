const AWS = require('aws-sdk');
const ec2 = new AWS.EC2({ region: process.env.AWS_REGION });

module.exports.initInstances = (instanceIds) => {
    return ec2.startInstances({ InstanceIds: [...instanceIds] }).promise();
};

module.exports.stopInstances = (instanceIds) => {
    return ec2.stopInstances({ InstanceIds: [...instanceIds] }).promise();
};

module.exports.ec2Instance = ({
    ENABLED: false,
    INSTANCE: 'NO INSTANCE'
});