const AWS = require('aws-sdk');
const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });

module.exports.sendToSQS = async (item) => {
    const params = {
        MessageBody: JSON.stringify(item),
        QueueUrl: process.env.AWS_SQS_QUEUE_URL
    };
    return await sqs.sendMessage(params).promise();
};