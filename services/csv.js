const { mongoConnection } = require('../db/mongo');
const paymentsService = require('../services/payments');
const ftpService = require('../services/ftp');
const cSVUtil = require('../utils/csv');
const { startDate, endDate, getUTC } = require('../utils/dateFormat');

module.exports.sendCSVFile = async () => {
    try {
        await mongoConnection();
        const payments = await paymentsService.getPaymentsByDate({
            startDate: getUTC(startDate),
            endDate: getUTC(endDate),
        });
        const csvCreated = cSVUtil.createCSV(payments);
        let ftpResp = 'isDisabled';

        if (JSON.parse(process.env.FTP_ENABLED)) {
            ftpResp = await ftpService.uploadFile(csvCreated.fileName);
        }
        const fileRemoved = cSVUtil.removeFile(csvCreated.fileName);

        return { csvCreated, ftpResp, fileRemoved };
    } catch (err) {
        throw new Error(err);
    }
};

