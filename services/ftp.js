const { Client } = require('basic-ftp');
const ftpClient = new Client();
ftpClient.ftp.verbose = true;

const { currenPath } = require('../utils/csv');

const getConnection = () => {
    return ftpClient.access({
        host: process.env.FTP_HOST,
        user: process.env.FTP_USER,
        password: process.env.FTP_PWD,
        secure: false,
    });
};

module.exports.closeConnection = () => {
    return ftpClient.close();
};

module.exports.getDirectoryList = async () => {
    await getConnection();
    return ftpClient.list();
};

module.exports.uploadFile = async (file) => {
    await getConnection();
    const fileUploaded = await ftpClient.uploadFrom(`${currenPath}/${file}`, `/public_html/transactions/${file}`);
    closeConnection();
    return {
        success: true,
        message: 'File uploaded',
        fileUploaded
    };
};

module.exports.downloadFile = async (file) => {
    await getConnection();
    const fileDownloaded = await ftpClient.downloadTo(`${currenPath}/${file}`, `/public_html/transactions/${file}`);
    closeConnection();
    return {
        success: true,
        message: 'File downloaded',
        fileDownloaded
    };
};