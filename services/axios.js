const axios = require('axios').default;
const FormData = require('form-data');
const fs = require('fs');

module.exports.sendFile = async () => {
    const formdata = new FormData();
    const localImg = fs.createReadStream('holi.jpeg');
    formdata.append('file', localImg);
    const formHeaders = formdata.getHeaders();
    const { data } = await axios.post(
        process.env.EP_SENDFILE,
        formdata,
        {
            headers: { ...formHeaders }
        });
    return data;
}