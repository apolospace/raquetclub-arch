const { mongoConnection } = require('../db/mongo');

module.exports.getPaymentsByDate = async ({ startDate, endDate }) => {
    const client = await mongoConnection();
    try {
        const Payment = client.db().collection('payments');
        const payments = await Payment.find({
            "actualDate": {
                "$gte": startDate,
                "$lte": endDate
            }
        }).toArray();
        if (payments.length == 0) {
            throw new Error('Payments not founded');
        }
        return payments;
    } catch (err) {
        throw new Error(`Get Payments ${err}`);
    } finally {
        client.close();
    }
};
