const { initInstances, stopInstances, ec2Instance } = require('./services/ec2');
const s3Service = require('./services/s3');
const { response } = require('./utils/response');

module.exports.startMongoInstance = async event => {
  try {
    if (JSON.parse(process.env.AWS_CW_CRON_ENABLED)) {
      ec2Instance.ENABLED = true;
      ec2Instance.INSTANCE = await initInstances([process.env.AWS_EC2_INSTANCEID]);
      console.log('Mongo Start instance [Success]', JSON.stringify(startInstance));
    }
    return response(200, true, 'OK', { ec2Instance });
  } catch (err) {
    console.log('Mongo Start instance [Error]', err.stack);
    return response(400, false, 'Err', err.message);
  }
};

module.exports.stopMongoInstance = async event => {
  try {
    if (JSON.parse(process.env.AWS_CW_CRON_ENABLED)) {
      ec2Instance.ENABLED = true;
      ec2Instance.INSTANCE = await stopInstances([process.env.AWS_EC2_INSTANCEID]);
      console.log('Mongo Stop [Success]', JSON.stringify(stopInstance));
    }
    return response(200, true, 'OK', { ec2Instance });
  } catch (err) {
    console.log('Mongo Stop [Error]', err.stack);
    return response(400, false, 'Err', err.message);
  }
};

module.exports.receiverSqs = (event, context) => {
  const message = event.Records[0];
  const data = JSON.parse(message.body);
  console.log('receiverSqs [Success]', data);
  context.done(null, '');
};

module.exports.createPresignedUrl = async event => {
  const { image } = JSON.parse(event.body);
  const urlData = s3Service.createPresignedUrl(image);
  return response(200, true, 'Presigned url created', urlData);
};
