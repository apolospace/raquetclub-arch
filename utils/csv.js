const { Parser } = require('json2csv');
const fs = require('fs');

const csvFields = ['paymentId', 'description', 'amount', 'currency'];
const json2csvParser = new Parser({ fields: csvFields });

// export const currenPath = './perra';
module.exports.currenPath = '/tmp';

module.exports.createCSV = (data) => {
    const csvData = json2csvParser.parse(data);
    const fileName = `doc-tx-concil-${new Date().getTime()}.csv`;
    fs.writeFile(`${currenPath}/${fileName}`, csvData, (err) => {
        if (err) {
            console.log(`Create CSVFile [Error] ${err}`);
        }
    });
    return {
        success: true,
        message: 'CSV created',
        fileName
    };
};

module.exports.removeFile = (file) => {
    try {
        fs.unlinkSync(`${currenPath}/${file}`);
        return {
            success: true,
            message: 'File removed'
        };
    } catch (err) {
        throw new Error(`Remove File [Error] ${err}`);
    }
};
