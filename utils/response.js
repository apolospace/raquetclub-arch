module.exports.response = (statusCode, success, message, data = {}) => {
    return {
        statusCode,
        body: JSON.stringify({
            statusCode,
            success,
            message,
            data
        }, null, 2)
    };
};